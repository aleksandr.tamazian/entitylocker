package locker;

import locker.exceptions.EntityLockerAcquireException;
import locker.task.EntityLockerTask;

/**
 * Interface for providing lock mechanism by value of entity ID.
 *
 * @param <T> type of entity ID.
 */
public interface EntityLocker<T> {

    /**
     * Provide lock for entity's ID by value.
     *
     * @param id   Id of entity (reference type)
     * @param task Instruction for execution in thread-safe manner
     * @throws EntityLockerAcquireException Acquire lock related exception
     */
    void tryTaskWithLock(T id, EntityLockerTask task) throws EntityLockerAcquireException;

    /**
     * Provide lock for entity's ID by value with specific timeout.
     *
     * @param id      Id of entity (reference type)
     * @param timeout Timeout of lock acquire
     * @param task    Instruction for execution in thread-safe manner
     * @throws InterruptedException         Timeout related exception
     * @throws EntityLockerAcquireException Acquire lock related exception
     */
    void tryTaskWithLock(T id, long timeout, EntityLockerTask task) throws InterruptedException, EntityLockerAcquireException;

}
