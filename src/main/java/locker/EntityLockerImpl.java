package locker;

import locker.exceptions.EntityLockerAcquireException;
import locker.task.EntityLockerTask;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class EntityLockerImpl<T> implements EntityLocker<T> {

    /**
     * I'm used weak hash map as alternative for LRU cache (https://github.com/google/guava/wiki/CachesExplained).
     * My point of usage is next one: weak hash map guarantees, that unused entries will be removed via GC.
     * <p>
     * Also, here might be used ConcurrentHashMap, but we have problem of such approach: correct and safe removal of unused entries.
     * Because ConcurrentHashMap doesn't support weak references, I'm tried to find alternative solution, but it's
     * kinda overhead, because for my own implementation of LRU cache I will spend a lot of time :)
     * <p>
     * But for ConcurrentHashMap, such approach might be fine with wrapper of lock with atomic counters for state handling.
     * <p>
     * To provide thread-safe usage, I'm used read/write lock. It's not a perfect approach, yep,
     * because locking works not for key's bucket, but just for the whole collection (in case of concurrent reads & writes).
     * At least I want to say, that it's better that nothing in my current situation, when I have crazy time limits.
     */
    private final Map<T, ReentrantLock> monitors = new WeakHashMap<>();

    private final ReadWriteLock internalReadWriteLock = new ReentrantReadWriteLock();

    @Override
    public void tryTaskWithLock(T id, EntityLockerTask task) throws EntityLockerAcquireException {
        if (id == null || task == null) {
            throw new EntityLockerAcquireException();
        }

        if (getLockObjectById(id) == null) {
            putLockObjectById(id);
        }

        Lock lock = getLockObjectById(id);
        // Oracle docs recommendations about ReentrantLock usage:
        // >It is recommended practice to always immediately follow a call to lock with a try block, most typically in a before/after construction
        // https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReentrantLock.html
        lock.lock();
        try {
            task.compute();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void tryTaskWithLock(T id, long timeout, EntityLockerTask task) throws InterruptedException, EntityLockerAcquireException {
        if (id == null || task == null) {
            throw new EntityLockerAcquireException();
        }

        if (timeout <= 0) {
            throw new EntityLockerAcquireException("Timeout cannot be negative or zero");
        }

        if (getLockObjectById(id) == null) {
            putLockObjectById(id);
        }

        Lock lock = getLockObjectById(id);
        boolean isAcquired = lock.tryLock(timeout, TimeUnit.MILLISECONDS);
        if (!isAcquired) {
            throw new EntityLockerAcquireException("Waiting time elapsed before the lock was acquired: " + timeout + " ms");
        }

        try {
            task.compute();
        } finally {
            lock.unlock();
        }
    }

    private ReentrantLock getLockObjectById(T id) {
        Lock readLock = internalReadWriteLock.readLock();
        readLock.lock();
        try {
            return monitors.get(id);
        } finally {
            readLock.unlock();
        }
    }

    private void putLockObjectById(T id) {
        Lock writeLock = internalReadWriteLock.writeLock();
        writeLock.lock();
        try {
            monitors.putIfAbsent(id, new ReentrantLock());
        } finally {
            writeLock.unlock();
        }
    }
}
