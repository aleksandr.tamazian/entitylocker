package locker.task;

/**
 * Functional interface for Entity Locker, represents amount of work, which might be associated as "non-thread safe code".
 */
public interface EntityLockerTask {
    void compute();
}
