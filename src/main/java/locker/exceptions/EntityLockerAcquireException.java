package locker.exceptions;

public class EntityLockerAcquireException extends RuntimeException {
    public EntityLockerAcquireException() {
        super("Method arguments must be not-null");
    }

    public EntityLockerAcquireException(String msg) {
        super(msg);
    }
}
