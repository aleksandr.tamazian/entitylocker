package locker.benchmark;

import locker.EntityLocker;
import locker.EntityLockerImpl;
import locker.tools.NonSafeInt;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
public class EntityLockerBenchmarks {

    private EntityLocker<String> stringLocker;
    private String entityIdValue;
    private NonSafeInt nonSafeInt;
    private ExecutorService executorService;

    @Setup
    public void setup() {
        stringLocker = new EntityLockerImpl<>();
        entityIdValue = "some-id";
        nonSafeInt = new NonSafeInt();
        executorService = Executors.newFixedThreadPool(2);
    }

    @Benchmark
    public void testMethod(Blackhole blackhole) {
        for (int i = 0; i < 100000; i++) {
            executorService.submit(() -> stringLocker.tryTaskWithLock(entityIdValue, () -> nonSafeInt.increment()));
            executorService.submit(() -> stringLocker.tryTaskWithLock(entityIdValue, () -> nonSafeInt.decrement()));
        }
        try {
            executorService.awaitTermination(3000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        blackhole.consume(nonSafeInt.value);
    }

    @Test
    public void launchBenchmark() throws Exception {
        Options opt = new OptionsBuilder()
                .include(this.getClass().getName() + ".*")
                .mode(Mode.AverageTime)
                .timeUnit(TimeUnit.MICROSECONDS)
                .warmupTime(TimeValue.seconds(3))
                .warmupIterations(10)
                .measurementTime(TimeValue.seconds(6))
                .measurementIterations(20)
                .threads(1)
                .forks(1)
                .shouldFailOnError(true)
                .shouldDoGC(true)
                .build();

        new Runner(opt).run();
    }
}
