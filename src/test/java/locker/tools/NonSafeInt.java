package locker.tools;

/**
 * Dummy class for tests verifications.
 */
public class NonSafeInt {
    public int value = 0;

    public void increment() {
        value++;
    }

    public void decrement() {
        value--;
    }
}
