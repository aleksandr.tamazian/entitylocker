package locker;

import locker.exceptions.EntityLockerAcquireException;
import locker.tools.NonSafeInt;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;

public class EntityLockerTest {

    private final int ENTITY_LOCKER_TIMEOUT = 3000;

    private final EntityLocker<String> stringLocker = new EntityLockerImpl<>();
    private final String entityIdValue = "some-id";
    private final NonSafeInt nonSafeInt = new NonSafeInt();
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Test
    public void testLockCorrectness() throws InterruptedException {
        testWithLocker(entityIdValue, entityIdValue);
        executorService.awaitTermination(ENTITY_LOCKER_TIMEOUT, TimeUnit.MILLISECONDS);
        assertThat(nonSafeInt.value).isEqualTo(0);
    }

    @Test
    public void testLockCorrectnessWithDifferentKeys() throws InterruptedException {
        testWithLocker(entityIdValue + "-1", entityIdValue + "-2");
        executorService.awaitTermination(ENTITY_LOCKER_TIMEOUT, TimeUnit.MILLISECONDS);
        assertThat(nonSafeInt.value).isNotEqualTo(0);
        nonSafeInt.value = 0;
    }

    @Test

    public void testLockCorrectnessWithMultipleIterations() throws InterruptedException {
        for (int i = 0; i < 100000; i++) {
            executorService.submit(() ->
                    stringLocker.tryTaskWithLock(entityIdValue, nonSafeInt::increment));

            executorService.submit(
                    nonSafeInt::decrement);
        }
        executorService.awaitTermination(ENTITY_LOCKER_TIMEOUT, TimeUnit.MILLISECONDS);
        assertThat(nonSafeInt.value).isEqualTo(0);
    }

    @Test
    public void testLockReentrancy() throws InterruptedException, EntityLockerAcquireException {
        AtomicBoolean isReentrant = new AtomicBoolean(false);
        stringLocker.tryTaskWithLock(entityIdValue, () ->
                stringLocker.tryTaskWithLock(entityIdValue, () -> isReentrant.set(true)));

        Thread.sleep(ENTITY_LOCKER_TIMEOUT);
        assertThat(isReentrant.get()).isTrue();
    }

    private void testWithLocker(String first, String second) {
        executorService.submit(() -> stringLocker.tryTaskWithLock(first, () -> {
            for (int i = 0; i < 100000; i++) {
                nonSafeInt.increment();
            }
        }));

        executorService.submit(() -> stringLocker.tryTaskWithLock(second, () -> {
            for (int i = 0; i < 100000; i++) {
                nonSafeInt.decrement();
            }
        }));
    }
}
